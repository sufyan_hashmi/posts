<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'title',
        'detail',
     ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }


    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            if(Auth::id())
                $model->user_id = Auth::id();

        });
        static::updating(function($model)
        {
            if(Auth::id())
             $model->user_id = Auth::id();
        });
    }
}
