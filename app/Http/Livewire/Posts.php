<?php

namespace App\Http\Livewire;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Posts extends Component
{

    use WithPagination;

    public $title, $detail, $post_id,$post,$search_post,$total_posts,$tags_input,$tags;
    public $updateMode = false;
    public $tag_id = false;

    public function render()
    {
        //filter by title
        $search_post = '%'.$this->search_post.'%';
        $posts = Post::where('title','Like',$search_post);


        //filter by tag
        $tag_id = $this->tag_id;
        if($tag_id)
            $posts->whereHas('tags', function ($q) use($tag_id)  {
                $q->where('tag_id', $tag_id);
            });

        //filter by login User
        if(Auth::user())
            $posts = $posts->where('user_id',Auth::id());

        //total posts
        $this->total_posts = $posts->count();

        $posts= $posts->paginate(5);

        //get all tags
        $this->tags = Tag::all();

        return view('livewire.posts',compact('posts'));
    }


    private function resetInputFields(){
        $this->title = '';
        $this->detail = '';
        $this->tags_input = '';

    }
    protected $rules = [
        'title' => 'required',
        'detail' => 'required',
    ];

    protected $messages = [
        'title.required' => 'The Title cannot be empty.',
        'detail.required' => 'The Detail cannot be empty.',

    ];


    public function store()
    {
        $validatedDate = $this->validate();

        $post= Post::create([
             'title' => $validatedDate['title'],
            'detail' => $validatedDate['detail'],
        ]);

        //create tags against post
        if($post)
        {
            $tagNames = explode(',',$this->tags_input);
            $tagIds = [];
            foreach($tagNames as $tagName)
            {
                //$post->tags()->create(['name'=>$tagName]);
                //Or to take care of avoiding duplication of Tag
                //you could substitute the above line as
                $tag = Tag::firstOrCreate(['name'=>$tagName]);
                if($tag)
                {
                    $tagIds[] = $tag->id;
                }

            }
            $post->tags()->sync($tagIds);

            $this->tag_id=false;
        }

        session()->flash('message', 'Post Created Successfully.');

        $this->resetInputFields();

    }

    public function edit($id)
    {
        $this->updateMode = true;

        $post = Post::where('id',$id)->first();
        $this->post_id = $id;
        $this->title = $post->title;
        $this->detail = $post->detail;
        $this->tags_input = implode(',', $post->tags()->pluck('name')->toArray());

    }


    public function cancel()
    {
        $this->updateMode = false;
         $this->resetInputFields();
    }

    public function update()
    {
         $this->validate();

        if ($this->post_id) {
             $post = Post::find($this->post_id);
            $post->update([
                'title' => $this->title,
                'detail' => $this->detail,
            ]);

            //update tags against post
            if($post)
            {
                $tagNames = explode(',',$this->tags_input);
                $tagIds = [];
                foreach($tagNames as $tagName)
                {
                    //$post->tags()->create(['name'=>$tagName]);
                    //Or to take care of avoiding duplication of Tag
                    //you could substitute the above line as
                    $tag = Tag::firstOrCreate(['name'=>$tagName]);
                    if($tag)
                    {
                        $tagIds[] = $tag->id;
                    }
                }
                $post->tags()->sync($tagIds);
                $this->tag_id=false;
            }

            $this->updateMode = false;
            session()->flash('message', 'Post Updated Successfully.');

            $this->resetInputFields();
            $this->tag_id=false;
        }

    }

    public function delete($id)
    {
        if($id){
            Post::where('id',$id)->delete();
            session()->flash('message', 'Post Deleted Successfully.');
        }
    }
    public function tagPosts($id)
    {
        if($id){
            $this->tag_id = $id;
            $tag = Tag::find($id);

            //tag count
            $this->total_posts = $tag->posts()->count();

            $this->render();
        }
    }
}
