<h3>Create Post </h3>
<hr>
<form>
    <div class="form-group">
        <label for="exampleFormControlInput1">Title</label>
        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Title" wire:model="title">
        @error('title') <span class="text-danger">{{ $message }}</span>@enderror
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput2">Detail</label>
        <textarea type="text" class="form-control" id="exampleFormControlInput2" wire:model="detail" placeholder="Enter Detail"></textarea>
        @error('detail') <span class="text-danger">{{ $message }}</span>@enderror
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput3">Tags(,)</label>
        <input type="text" class="form-control" id="exampleFormControlInput3" placeholder="Enter Tags(,)" wire:model="tags_input">
        @error('tags_input') <span class="text-danger">{{ $message }}</span>@enderror
    </div>

    <button wire:click.prevent="store()" class="btn btn-success">Save</button>
</form>