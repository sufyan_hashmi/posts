<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Users</h2>
                </div>
                <div class="card-body">
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                        <div>
                            @if($updateMode)
                                @include('users.update')
                            @elseif($viewMode)
                                @include('users.view')
                            @else
                                @include('users.create')
                            @endif
                            <table class="table table-bordered mt-5">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $value)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>
                                            <button wire:click="show({{ $value->id }})" class="btn btn-warning btn-sm">View</button>
                                            <button wire:click="edit({{ $value->id }})" class="btn btn-primary btn-sm">Edit</button>
                                            <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                                {{ $users->links() }}

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

