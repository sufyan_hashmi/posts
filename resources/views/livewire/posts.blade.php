
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2>Posts</h2>
            </div>
            <div class="card-body">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                <div>
                    @hasrole('Admin|User')
                        @if($updateMode)
                            @include('posts.update')
                        @else
                            @include('posts.create')
                        @endif
                    @endhasrole
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Tags</h3>
                            <a href="#" wire:click="tagPosts({{ 1 }})" class="badge badge-info"></a>

                        @foreach($tags as $tag)
                                <a href="#" wire:click="tagPosts({{  $tag->id }})" class="badge badge-info">{{$tag->name}}</a>
                            @endforeach
                        </div>

                    </div>
                    <div class="row">
                            <div class="col-md-9">
                             </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="search_post" placeholder="Search Post" wire:model="search_post">
                            </div>
                         </div>


                    <span>Total Posts : {{$total_posts }} </span>
                    <table class="table table-bordered mt-5">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Detail</th>

                            @hasrole('Admin|User')

                            <th>Action</th>
                            @endhasrole
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{  $post->title }}</td>
                                <td>{{  $post->detail }}</td>
                                @hasrole('Admin|User')
                                    <td>
                                        <button wire:click="edit({{  $post->id }})" class="btn btn-primary btn-sm">Edit</button>
                                        <button wire:click="delete({{  $post->id }})" class="btn btn-danger btn-sm">Delete</button>
                                    </td>
                                @endhasrole
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $posts->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
</div>