<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $roles =[
           ['name' => 'Admin'],
           ['name' => 'User']
       ];

       foreach ($roles as $role)
             Role::create($role);

       $admin = Role::where('name','Admin')->first();
        $admin->syncPermissions(Permission::all());

    }
}
