<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions =[
            ['name' => 'User Management'],
            ['name' => 'Post Management'],
            ['name' => 'Create Post'],
            ['name' => 'Edit Post'],
            ['name' => 'Delete Post'],
        ];

        foreach ($permissions as $permission)
            Permission::create($permission);
    }
}
