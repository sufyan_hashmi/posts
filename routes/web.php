<?php

use Illuminate\Support\Facades\Route;
 use \App\Http\Livewire\Users;
 use \App\Http\Livewire\Posts;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Posts::class)->name('posts');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', Posts::class)->name('dashboard');



Route::group(['middleware' => ['role:Admin']], function () {
    Route::get('user',  Users::class)->name('users')->middleware('auth');
});


Route::view('/home', 'users.home');
